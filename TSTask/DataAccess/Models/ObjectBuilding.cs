﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataAccess.Models
{
    [Table("ObjectBuilding")]
    public class ObjectBuilding
    {
        [Key]
        public int ObjectId { get; set; }

        public string Name { get; set; }
        public string Adress{ get; set; }
        public string City { get; set; }

        public ICollection<Room> Rooms { get; set; }

    }
}
