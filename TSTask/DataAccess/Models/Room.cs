﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Models
{
    [Table("Room")]
    public class Room
    {
        [Key]
        public int RoomId { get; set; }
        public string RoomName { get; set; }
        public string RoomDescription { get; set; }


        [ForeignKey("ObjectBuilding")]
        public int ObjectId { get; set; }
        public ObjectBuilding ObjectBuilding { get; set; }

        public ICollection<Reservation> Reservations { get; set; }

    }
}
