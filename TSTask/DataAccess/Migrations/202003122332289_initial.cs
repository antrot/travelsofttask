namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Guest",
                c => new
                    {
                        GuestID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.GuestID);
            
            CreateTable(
                "dbo.Reservation",
                c => new
                    {
                        ReservationId = c.Int(nullable: false, identity: true),
                        DateFrom = c.DateTime(nullable: false),
                        DateTo = c.DateTime(nullable: false),
                        GuestId = c.Int(nullable: false),
                        RoomId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ReservationId)
                .ForeignKey("dbo.Guest", t => t.GuestId, cascadeDelete: true)
                .ForeignKey("dbo.Room", t => t.RoomId, cascadeDelete: true)
                .Index(t => t.GuestId)
                .Index(t => t.RoomId);
            
            CreateTable(
                "dbo.Room",
                c => new
                    {
                        RoomId = c.Int(nullable: false, identity: true),
                        RoomName = c.String(),
                        RoomDescription = c.String(),
                        ObjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RoomId)
                .ForeignKey("dbo.ObjectBuilding", t => t.ObjectId, cascadeDelete: true)
                .Index(t => t.ObjectId);
            
            CreateTable(
                "dbo.ObjectBuilding",
                c => new
                    {
                        ObjectId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Adress = c.String(),
                        City = c.String(),
                    })
                .PrimaryKey(t => t.ObjectId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reservation", "RoomId", "dbo.Room");
            DropForeignKey("dbo.Room", "ObjectId", "dbo.ObjectBuilding");
            DropForeignKey("dbo.Reservation", "GuestId", "dbo.Guest");
            DropIndex("dbo.Reservation", new[] { "RoomId" });
            DropIndex("dbo.Room", new[] { "ObjectId" });
            DropIndex("dbo.Reservation", new[] { "GuestId" });
            DropTable("dbo.ObjectBuilding");
            DropTable("dbo.Room");
            DropTable("dbo.Reservation");
            DropTable("dbo.Guest");
        }
    }
}
