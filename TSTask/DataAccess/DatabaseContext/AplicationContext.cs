﻿using System.Data.Entity;
using DataAccess.Models;
 
namespace DataAccess.DatabaseContext
{
    public class AplicationContext : DbContext
    {
        public AplicationContext() : base("TSDataBaseConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AplicationContext, Migrations.Configuration>("TSDataBaseConnection"));
        }
        
        // Entities        
        public DbSet<ObjectBuilding> ObjectBuildings { get; set; }
        public DbSet<Guest> Guests { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Reservation> Reservations { get; set; }

    }
}
