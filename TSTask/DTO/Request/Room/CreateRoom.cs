﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Request.Room
{
    public class CreateRoom
    {
        public string RoomName { get; set; }
        public string RoomDescription { get; set; }
        public string ObjectId { get; set; }
    }
}
