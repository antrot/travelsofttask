﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Request.Room
{
    public class UpdateRoom
    {
        public string Value { get; set; }
        public string RoomName { get; set; }
        public string RoomDescription { get; set; }

    }
}
