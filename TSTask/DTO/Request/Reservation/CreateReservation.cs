﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Request.Reservation
{
    public class CreateReservation
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string GuestId { get; set; }
        public string RoomId { get; set; }
    }
}
