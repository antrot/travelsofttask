﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Request.Reservation
{
    public class UpdateReservation
    {
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }

    }
}
