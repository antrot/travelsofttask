﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Request.ObjectBuilding
{
    public class UpdateObjectBuilding
    {
        public string Name { get; set; }
        public string Adress { get; set; }
        public string City { get; set; }


    }
}
