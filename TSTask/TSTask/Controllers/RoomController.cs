﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace TSTask.Controllers
{
    [RoutePrefix("Aplication/api/v1/Room")]

    public class RoomController : ApiController
    {

        [Route("GetAll")]
        [HttpGet]
        public async Task<HttpResponseMessage>  GetAll()
        {
            return null;
        }

        [Route("Get/{id}")]
        [HttpGet]
        public async Task<HttpResponseMessage> Get(int id)
        {
            return null;
        }

        [Route("CreateNew")]
        [HttpPost]
        public async Task<HttpResponseMessage> CreateNew( string value)
        {
            return null;

        }

        [Route("Update/{id}")]
        [HttpPost]
        public async Task<HttpResponseMessage> Update(int id, string value)
        {
            return null;
        }

        [Route("Delete/{id}")]
        [HttpGet]
        public async Task<HttpResponseMessage> Delete(int id)
        {
            return null;

        }
    }
}