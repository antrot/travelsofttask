﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TSTaskApi._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   <div>
        <style type="text/css">
            .style1 {
                font-weight: normal;
            }
        </style>
        <h2 class="style1">Sve rezervacije sa detaljima objekta itd</h2>

        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" CssClass="mydatagrid" PagerStyle-CssClass="pager" AllowSorting="False" HeaderStyle-CssClass="header" RowStyle-CssClass="rows"  AutoGenerateColumns="False"
            DataKeyNames="ReservationID"
           Width="100%">
            <Columns>
                <asp:BoundField DataField="ReservationID" HeaderText="ReservationID" SortExpression="ReservationID" />
                <asp:BoundField DataField="DateFrom" HeaderText="Date From" SortExpression="DateFrom" />
                <asp:BoundField DataField="DateTo" HeaderText="Date To" SortExpression="Date To" />
                <asp:BoundField DataField="UserName" HeaderText="Guest Username" SortExpression="UserName" />
                <asp:BoundField DataField="FirstName" HeaderText="Guest First Name" SortExpression="FirstName" />
                <asp:BoundField DataField="LastName" HeaderText="Guest Last Name" SortExpression="LastName" />
                <asp:BoundField DataField="RoomName" HeaderText="Room Name" SortExpression="RoomName" />
                <asp:BoundField DataField="RoomDescription" HeaderText="Room description" SortExpression="RoomDescription" />
                <asp:BoundField DataField="Name" HeaderText="Object Name" SortExpression="Name" />
                <asp:BoundField DataField="City" HeaderText="Object City" SortExpression="City" />
                <asp:BoundField DataField="Adress" HeaderText="Object adress" SortExpression="Adress" />
            </Columns>
        </asp:GridView>
    </div>
    
</asp:Content>
