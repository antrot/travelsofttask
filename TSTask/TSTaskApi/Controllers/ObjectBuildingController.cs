﻿using DataAccess.Models;
using DTO.Request.ObjectBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace TSTaskApi.Controllers
{
    [RoutePrefix("api/v1/ObjectBuilding")]

    public class ObjectBuildingController : ApiController
    {
        public Service.ObjectBuilding service;

        public ObjectBuildingController()
        {
            service = new Service.ObjectBuilding();
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<List<ObjectBuilding>> GetAll()
        {
            var allBuildings = await service.GetAll();
            return allBuildings;
        }

        [Route("Get/{id}")]
        [HttpGet]
        public async Task<ObjectBuilding> Get(string id)
        {
            var objectBuilding = await service.Get(id);
            return objectBuilding;
        }

        [Route("CreateNew")]
        [HttpPost]
        public async Task<HttpResponseMessage> CreateNew(CreateObjectBuilding value)
        {
            try
            {
                var CreadteRoom = await service.Create(value);
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK };
            }
            catch
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError };
            }

        }

        [Route("Update/{id}")]
        [HttpPost]
        public async Task<HttpResponseMessage> Update(int id, UpdateObjectBuilding value)
        {
            try
            {
                var CreadteRoom = await service.Upadate(id.ToString(),value);
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK };
            }
            catch
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError };
            }

        }

        [Route("Delete/{id}")]
        [HttpGet]
        public async Task<HttpResponseMessage> Delete(string id)
        {
            try
            {
                var deleteObject = await service.Delete(id);
                if (deleteObject.Response.ToLower() == "sucess")
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK };
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError };
                }
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                return response;
            }
        }
    }
}