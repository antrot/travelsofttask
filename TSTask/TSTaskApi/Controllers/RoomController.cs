﻿using DataAccess.Models;
using DTO.Request.Room;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace TSTaskApi.Controllers
{
    [RoutePrefix("api/v1/Room")]
    public class RoomController : ApiController
    {
        public Service.Room service;

        public RoomController()
        {
            service = new Service.Room();
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<List<Room>> GetAll()
        {
            var allRooms = await service.GetAll();
            return allRooms;
        }

        [Route("Get/{id}")]
        [HttpGet]
        public async Task<Room> Get(string id)
        {
            var singleRoom = await service.Get(id);
            return singleRoom;
        }

        [Route("CreateNew")]
        [HttpPost]
        public async Task<HttpResponseMessage> CreateNew(CreateRoom value)
        {
            try
            {
                var CreadteRoom = await service.Create(value);
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK };
            }
            catch
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError };
            }
        }

        [Route("Update/{id}")]
        [HttpPost]
        public async Task<HttpResponseMessage> Update(int id, UpdateRoom value)
        {
            try
            {
                var updateRoom = await service.Upadate(id.ToString(), value);
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK };
            }
            catch
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError };
            }

        }

        [Route("Delete/{id}")]
        [HttpGet]
        public async Task<HttpResponseMessage> Delete(string id)
        {
            try
            {
                var deleteRoom = await service.Delete(id);
                if (deleteRoom.Response.ToLower() == "sucess")
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK };
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError };
                }
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                return response;
            }

        }
    }
}