﻿using DataAccess.Models;
using DTO.Request.Reservation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace TSTaskApi.Controllers
{
    [RoutePrefix("api/v1/Reservation")]
    public class ReservationController : ApiController
    {
        public Service.Reservation service;

        public ReservationController()
        {
            service = new Service.Reservation();
        }
        [Route("GetAll")]
        [HttpGet]
        public async Task<List<Reservation>> GetAll()
        {
            var allReservations = await service.GetAll();
            return allReservations;
        }

        [Route("Get/{id}")]
        [HttpGet]
        public async Task<Reservation> Get(string id)
        {
            var singleReservation = await service.Get(id);
            return singleReservation;
        }

        [Route("CreateNew")]
        [HttpPost]
        public async Task<HttpResponseMessage> CreateNew(CreateReservation value)
        {
            try
            {
                var createdReservation = await service.Create(value);
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK };
            }
            catch
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError };
            }

        }

        [Route("Update/{id}")]
        [HttpPost]
        public async Task<HttpResponseMessage> Update(int id, UpdateReservation value)
        {
            try
            {
                var updateReservation = await service.Upadate(id.ToString(), value);
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK };
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                return response;
            }
        }

        [Route("Delete/{id}")]
        [HttpGet]
        public async Task<HttpResponseMessage> Delete(string id)
        {
            try
            {
                var deleteReservation = await service.Delete(id);
                if (deleteReservation.Response.ToLower() == "sucess")
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK };
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError };
                }
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                return response;
            }

        }
    }
}