﻿using DataAccess.Models;
using DTO.Request.Guest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace TSTaskApi.Controllers
{
    [RoutePrefix("api/v1/Guest")]

    public class GuestController : ApiController
    {
        public Service.Guest service;

        public GuestController()
        {
            service = new Service.Guest();
        }
        


        [Route("GetAll")]
        [HttpGet]
        public async Task<List<Guest>> GetAll()
        {
            var allGuests = await service.GetAll();
            return allGuests;

        }

        [Route("Get/{id}")]
        [HttpGet]
        public async Task<Guest> Get(string id)
        {
            var singleGuest = await service.Get(id);
            return singleGuest;
        }

        [Route("CreateNew")]
        [HttpPost]
        public async Task<HttpResponseMessage> CreateNew(CreateGuest value)
        {
            try
            {
                var CreatedGuest = await service.Create(value);
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK };
            }
            catch
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError };
            }
        }

        [Route("Update/{id}")]
        [HttpPost]
        public async Task<HttpResponseMessage> Update(int id, UpdateGuest value)
        {
            try
            {
                var guest = await service.Upadate(id.ToString(), value);
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK };
            }
            catch
            {
                return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError };
            }

        }

        [Route("Delete/{id}")]
        [HttpGet]
        public async Task<HttpResponseMessage> Delete(string id)
        {
          
            try
            {
                var deletedGuest = await service.Delete(id);
                if (deletedGuest.Response.ToLower() == "sucess")
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.OK };
                else
                {
                    return new HttpResponseMessage() { StatusCode = HttpStatusCode.InternalServerError };
                }
            }
            catch(Exception ex)
            {
                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                return response;
            }
        }
    }
}