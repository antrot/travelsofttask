﻿using DataAccess.DatabaseContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TSTaskApi.Controllers;

namespace TSTaskApi
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();
            CreateDatabseIfNotExists();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private static void CreateDatabseIfNotExists()
        {
            var controller = new GuestController();
            controller.GetAll();
        }
    }
}
