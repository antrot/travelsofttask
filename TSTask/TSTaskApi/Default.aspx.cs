﻿using DTO.Request.Reservation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TSTaskApi.Controllers;

namespace TSTaskApi
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                refreshdata();
            }

        }
        public void refreshdata()
        {
          
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSDataBaseConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand(@"select ReservationID,DateFrom,DateTo,b.UserName,b.FirstName,b.LastName,c.RoomName,c.RoomDescription,d.Name,d.City,d.Adress
                                              from Reservation as a
                                              inner join Guest as b on b.GuestID=a.GuestId 
                                              inner join Room as c on c.RoomId=a.RoomId
                                              inner join ObjectBuilding as d on c.ObjectId= d.ObjectId", connection);
            SqlDataAdapter sqlDataAtapter = new SqlDataAdapter(cmd);
            DataTable dataTable = new DataTable();
            sqlDataAtapter.Fill(dataTable);
            GridView1.DataSource = dataTable;
            GridView1.DataBind();


        }



       
    }
}