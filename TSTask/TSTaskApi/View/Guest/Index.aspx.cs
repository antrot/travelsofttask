﻿using DTO.Request.Guest;
using DTO.Request.ObjectBuilding;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TSTaskApi.Controllers;

namespace TSTaskApi.View.Guest
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                refreshdata();
            }

        }
        public void refreshdata()
        {
            txterror.Value = "";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSDataBaseConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("select * from Guest", connection);
            SqlDataAdapter sqlDataAtapter = new SqlDataAdapter(cmd);
            DataTable dataTable = new DataTable();
            sqlDataAtapter.Fill(dataTable);
            GridView1.DataSource = dataTable;
            GridView1.DataBind();


        }
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            var controller = new GuestController();
            foreach (var key in e.Keys)
            {
                var c = key;
            }

            int id = Convert.ToInt16(e.Keys[0].ToString());
            var result = controller.Delete(id.ToString());

            refreshdata();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            refreshdata();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            refreshdata();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                var controller = new GuestController();
                var valuest = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
                int id = Convert.ToInt16(valuest);
                UpdateGuest updateObjectBuilding = new UpdateGuest();

                updateObjectBuilding.FirstName = e.NewValues["FirstName"].ToString();
                updateObjectBuilding.LastName = e.NewValues["LastName"].ToString();
                updateObjectBuilding.UserName = e.NewValues["UserName"].ToString();

                if (updateObjectBuilding.FirstName == "" || updateObjectBuilding.LastName == "" || updateObjectBuilding.UserName == "")
                    txterror.Value = "FirstName or LastName or USername not entered";
                else
                {
                    var result = controller.Update(id, updateObjectBuilding);
                    if (!result.Result.IsSuccessStatusCode)
                    {
                        txterror.Value = "Guest with same userName already exist";
                    }
                    else
                    {
                        GridView1.EditIndex = -1;
                        refreshdata();
                    }
                    GridView1.EditIndex = -1;
                    refreshdata();
                }
            }
            catch (Exception ex)
            {
                txterror.Value = ex.Message;
            }
        }
        protected void Button1_Click(object sender, EventArgs e1)
        {
            var controller = new GuestController();
            var createObjectBuilding = new CreateGuest();
            if (txtFirstName.Value == "" || txtLastName.Value == "" || txtUserName.Value == "")
                txterror.Value = "FirstName or LastName or USername not entered";
            else
            {
                createObjectBuilding.FirstName = txtFirstName.Value.ToString(); ;
                createObjectBuilding.LastName = txtLastName.Value.ToString(); ;
                createObjectBuilding.UserName = txtUserName.Value.ToString();
                var result = controller.CreateNew(createObjectBuilding);
                if (!result.Result.IsSuccessStatusCode)
                {
                    txterror.Value = "Guest with same userName already exist";
                }
                else
                {
                    GridView1.EditIndex = -1;
                    refreshdata();
                }

            }

        }
    }
}