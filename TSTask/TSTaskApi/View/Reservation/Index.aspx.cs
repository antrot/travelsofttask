﻿using DTO.Request.ObjectBuilding;
using DTO.Request.Reservation;
using DTO.Request.Room;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TSTaskApi.Controllers;

namespace TSTaskApi.View.Reservation
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                refreshdata();
            }

        }
        public void refreshdata()
        {
            txterror.Value = "";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSDataBaseConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand(@"select ReservationID,DateFrom,DateTo,b.UserName,c.RoomName from Reservation as a 
                                                inner join Guest as b on b.GuestID=a.GuestId 
                                                inner join Room as c on c.RoomId=a.RoomId", connection);
            SqlDataAdapter sqlDataAtapter = new SqlDataAdapter(cmd);
            DataTable dataTable = new DataTable();
            sqlDataAtapter.Fill(dataTable);
            GridView1.DataSource = dataTable;
            GridView1.DataBind();

            FillObjectDropdown();

            FillRoomDropdown();


        }

        private void FillObjectDropdown()
        {
            RoomDropdown.Items.Clear();
            var newItemDefault = new ListItem();
            newItemDefault.Text = "Select Room";
            newItemDefault.Value = "-1";
            RoomDropdown.Items.Add(newItemDefault);


            SqlConnection connection2 = new SqlConnection(ConfigurationManager.ConnectionStrings["TSDataBaseConnection"].ConnectionString);
            SqlCommand comm = connection2.CreateCommand();
            comm = connection2.CreateCommand();
            comm.CommandText = "SELECT RoomName,RoomID from Room";
            comm.CommandType = CommandType.Text;
            connection2.Open();
            SqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                var newItem = new ListItem();
                newItem.Text = dr["RoomName"].ToString();
                newItem.Value = dr["RoomID"].ToString();
                RoomDropdown.Items.Add(newItem);
            }
        }

        private void FillRoomDropdown()
        {
            UserDropdown.Items.Clear();
            var newItemDefault = new ListItem();
            newItemDefault.Text = "Select user";
            newItemDefault.Value = "-1";
            UserDropdown.Items.Add(newItemDefault);



            SqlConnection connection2 = new SqlConnection(ConfigurationManager.ConnectionStrings["TSDataBaseConnection"].ConnectionString);
            SqlCommand comm = connection2.CreateCommand();
            comm = connection2.CreateCommand();
            comm.CommandText = "SELECT Username,GuestID from Guest";
            comm.CommandType = CommandType.Text;
            connection2.Open();
            SqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                var newItem = new ListItem();
                newItem.Text = dr["UserName"].ToString();
                newItem.Value = dr["GuestID"].ToString();
                UserDropdown.Items.Add(newItem);
            }
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ReservationController controller = new ReservationController();
            foreach (var key in e.Keys)
            {
                var c = key;
            }

            int id = Convert.ToInt16(e.Keys[0].ToString());
            var result = controller.Delete(id.ToString());

            refreshdata();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            refreshdata();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            refreshdata();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            ReservationController controller = new ReservationController();
            var valuest = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
            int id = Convert.ToInt16(valuest);
            UpdateReservation updateObjectBuilding = new UpdateReservation();
            try
            {
                updateObjectBuilding.DateFrom = (DateTime.Parse(e.NewValues["DateFrom"].ToString()));
                updateObjectBuilding.DateTo = (DateTime.Parse(e.NewValues["DateTo"].ToString()));
                var result = controller.Update(id, updateObjectBuilding);

                GridView1.EditIndex = -1;
                refreshdata();
            }
            catch (Exception ex)
            {
                txterror.Value = ex.Message;
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {

            ReservationController controller = new ReservationController();
            var createRoom = new CreateReservation();
            try
            {

                if (txtDateFrom.Value == "" || txtDateTo.Value == "")
                    txterror.Value = "Dates not selected";
                else
                {
                    createRoom.DateFrom = (DateTime.Parse(txtDateFrom.Value.ToString()));
                    createRoom.DateTo = (DateTime.Parse(txtDateFrom.Value.ToString()));
                    createRoom.RoomId = RoomDropdown.SelectedValue;
                    createRoom.GuestId = UserDropdown.SelectedValue;

                    if (createRoom.RoomId == "-1" || createRoom.GuestId == "-1")
                    {
                        txterror.Value = "Room or guest not selected,";
                    }
                    else
                    {
                        txterror.Value = "";
                        var result = controller.CreateNew(createRoom);
                        refreshdata();
                    }
                }
            }
            catch (Exception ex)
            {
                txterror.Value = ex.Message;
            }
        }
    }
}