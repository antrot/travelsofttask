﻿using DTO.Request.ObjectBuilding;
using DTO.Request.Room;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TSTaskApi.Controllers;

namespace TSTaskApi.View.Room
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                refreshdata();
            }

        }
        public void refreshdata()
        {
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["TSDataBaseConnection"].ConnectionString);
            SqlCommand cmd = new SqlCommand("select RoomID, RoomName, RoomDescription, Name from ObjectBuilding as b inner join Room as a on a.ObjectId = b.ObjectId", connection);
            SqlDataAdapter sqlDataAtapter = new SqlDataAdapter(cmd);
            DataTable dataTable = new DataTable();
            sqlDataAtapter.Fill(dataTable);
            GridView1.DataSource = dataTable;
            GridView1.DataBind();



            ListItem.Items.Clear();

            var newItemDefault = new ListItem();
            newItemDefault.Text = "SelectObject";
            newItemDefault.Value = "-1";
            ListItem.Items.Add(newItemDefault);

            SqlConnection connection2 = new SqlConnection(ConfigurationManager.ConnectionStrings["TSDataBaseConnection"].ConnectionString);
            SqlCommand comm = connection2.CreateCommand();
            comm = connection2.CreateCommand();
            comm.CommandText = "SELECT ObjectId,Name from ObjectBuilding";
            comm.CommandType = CommandType.Text;
            connection2.Open();
            SqlDataReader dr = comm.ExecuteReader();
            while (dr.Read())
            {
                var newItem = new ListItem();
                newItem.Text = dr["Name"].ToString();
                newItem.Value = dr["ObjectId"].ToString();
                ListItem.Items.Add(newItem);
            }


        }
        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            RoomController controller = new RoomController();
            foreach (var key in e.Keys)
            {
                var c = key;
            }

            int id = Convert.ToInt16(e.Keys[0].ToString());
            var result = controller.Delete(id.ToString());

            refreshdata();
        }

        protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            refreshdata();
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.EditIndex = -1;
            refreshdata();
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            RoomController controller = new RoomController();
            try
            {
                var valuest = GridView1.DataKeys[e.RowIndex].Values[0].ToString();
                int id = Convert.ToInt16(valuest);
                UpdateRoom updateObjectBuilding = new UpdateRoom();

                updateObjectBuilding.RoomName = e.NewValues["RoomName"].ToString();
                updateObjectBuilding.RoomDescription = e.NewValues["RoomDescription"].ToString();
                if (updateObjectBuilding.RoomName == "" || updateObjectBuilding.RoomDescription == "")
                    txterror.Value = "Name or description not entered";
                else
                {
                    var result = controller.Update(id, updateObjectBuilding);

                    GridView1.EditIndex = -1;
                    refreshdata();
                }
            }
            catch (Exception ex)
            {
                txterror.Value = ex.Message;
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {

            RoomController controller = new RoomController();
            var createRoom = new CreateRoom();
            if (txtRoomName.Value == "" || txtRoomDescription.Value == "")
                txterror.Value = "Name or description not entered";
            else
            {
                createRoom.RoomName = txtRoomName.Value.ToString(); ;
                createRoom.RoomDescription = txtRoomDescription.Value.ToString(); ;
                createRoom.ObjectId = ListItem.SelectedValue;
                if (ListItem.SelectedValue == "-1")
                    txterror.Value = "Object not selected";
                else
                {
                    txterror.Value = "";
                    var result = controller.CreateNew(createRoom);
                    refreshdata();
                }
            }


        }
    }
}