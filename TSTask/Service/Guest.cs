﻿using DataAccess.Models;
using DTO.Request.Guest;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class Guest : IGuest
    {
        public string connectionString = Properties.Settings.Default.connectionString;

        public async Task<DTO.Response.GenericResponse> Create(CreateGuest guest)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var single = ctx.Guests.Where(x => x.UserName == guest.UserName).FirstOrDefault();
                    if (single != null)
                        throw new Exception("Guest with same userName alreeady exists in database");

                    DataAccess.Models.Guest guestResult = new DataAccess.Models.Guest()
                    {
                        FirstName = guest.FirstName,
                        LastName = guest.LastName,
                        UserName=guest.UserName
                    };
                    ctx.Guests.Add(guestResult);
                    ctx.SaveChanges();
                    var response = new DTO.Response.GenericResponse();
                    response.Response = "Sucess";
                    return response;
                }
                catch (Exception ex)
                {
                    var response = new DTO.Response.GenericResponse();
                    response.Response = ex.Message;
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<DTO.Response.GenericResponse> Delete(string id)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var intID = int.Parse(id);
                    var single = ctx.Guests.Where(x => x.GuestID == intID).FirstOrDefault();
                    ctx.Entry(single).State = System.Data.Entity.EntityState.Deleted;
                    ctx.SaveChanges();

                    var response = new DTO.Response.GenericResponse();
                    response.Response = "Sucess";
                    return response;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<DataAccess.Models.Guest> Get(string id)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var intID = int.Parse(id);
                    var returnObjet = ctx.Guests.Where(x => x.GuestID == intID).FirstOrDefault();
                    return returnObjet;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<List<DataAccess.Models.Guest>> GetAll()
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var returnObjet = ctx.Guests.ToList();
                    return returnObjet;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<DataAccess.Models.Guest> Upadate(string id, UpdateGuest guest)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    int intID = int.Parse(id);
                    var updatedGuest = ctx.Guests.FirstOrDefault(x => x.UserName == guest.UserName);
                    if (updatedGuest != null && updatedGuest.UserName == guest.UserName)
                        throw new Exception("User with that username already exists");
                    var returnObjet = ctx.Guests.FirstOrDefault(x => x.GuestID == intID);
                    returnObjet.FirstName = guest.FirstName;
                    returnObjet.LastName = guest.LastName;
                    returnObjet.UserName = guest.UserName;
                    ctx.SaveChanges();
                    return returnObjet;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
