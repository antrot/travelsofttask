﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO.Request.Room;

namespace Service.Interface
{
    public interface IRoom
    {
        Task<List<DataAccess.Models.Room>> GetAll();
        Task<DataAccess.Models.Room> Get(string id);
        Task<DataAccess.Models.Room> Upadate(string id,UpdateRoom room);
        Task<DataAccess.Models.Room> Create(CreateRoom room);
        Task<DTO.Response.GenericResponse> Delete(string id);




    }
}
