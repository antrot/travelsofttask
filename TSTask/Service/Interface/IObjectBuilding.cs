﻿using DTO.Request.ObjectBuilding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IObjectBuilding
    {
        Task<List<DataAccess.Models.ObjectBuilding>> GetAll();
        Task<DataAccess.Models.ObjectBuilding> Get(string id);
        Task<DataAccess.Models.ObjectBuilding> Upadate(string id, UpdateObjectBuilding objectBuilding);
        Task<DTO.Response.GenericResponse> Create(CreateObjectBuilding objectBuilding);
        Task<DTO.Response.GenericResponse> Delete(string id);
    }
}
