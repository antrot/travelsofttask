﻿using DTO.Request;
using DTO.Request.Guest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IGuest
    {
        Task<List<DataAccess.Models.Guest>> GetAll();
        Task<DataAccess.Models.Guest> Get(string id);
        Task<DataAccess.Models.Guest> Upadate(string id, UpdateGuest guest);
        Task<DTO.Response.GenericResponse> Create(CreateGuest guest);
        Task<DTO.Response.GenericResponse> Delete(string id);
    }
}
