﻿using DTO.Request.Reservation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IReservation
    {
        Task<List<DataAccess.Models.Reservation>> GetAll();
        Task<DataAccess.Models.Reservation> Get(string id);
        Task<DataAccess.Models.Reservation> Upadate(string id, UpdateReservation guest);
        Task<DataAccess.Models.Reservation> Create(CreateReservation guest);
        Task<DTO.Response.GenericResponse> Delete(string id);
    }
}
