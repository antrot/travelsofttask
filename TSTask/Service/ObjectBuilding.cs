﻿using DataAccess.Models;
using DTO.Request.ObjectBuilding;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class ObjectBuilding : IObjectBuilding
    {
        public string connectionString = Properties.Settings.Default.connectionString;
        public async Task<DTO.Response.GenericResponse> Create(CreateObjectBuilding objectBuilding)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var single = ctx.ObjectBuildings.Where(x => x.Name == objectBuilding.Name).FirstOrDefault();
                    if (single != null)
                        throw new Exception("Building with same name alreeady exists in database");

                    DataAccess.Models.ObjectBuilding building = new DataAccess.Models.ObjectBuilding()
                    {
                        Adress = objectBuilding.Adress,
                        City = objectBuilding.City,
                        Name = objectBuilding.Name
                    };
                    ctx.ObjectBuildings.Add(building);
                    ctx.SaveChanges();
                    var response = new DTO.Response.GenericResponse();
                    response.Response = "Sucess";
                    return response;
                }
                catch (Exception ex)
                {
                    var response = new DTO.Response.GenericResponse();
                    response.Response = ex.Message;
                    throw new Exception(ex.Message);

                }
            }
        }

        public async Task<DTO.Response.GenericResponse> Delete(string id)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var intID = int.Parse(id);

                    var single = ctx.ObjectBuildings.Where(x => x.ObjectId == intID).FirstOrDefault();
                    ctx.Entry(single).State = System.Data.Entity.EntityState.Deleted;
                    ctx.SaveChanges();

                    var response = new DTO.Response.GenericResponse();
                    response.Response = "Sucess";
                    return response;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }

        }

        public async Task<DataAccess.Models.ObjectBuilding> Get(string id)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var intID = int.Parse(id);
                    var returnObjet = ctx.ObjectBuildings.Where(x => x.ObjectId == intID).FirstOrDefault();
                    return returnObjet;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<List<DataAccess.Models.ObjectBuilding>> GetAll()
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var returnObjet = ctx.ObjectBuildings.ToList();
                    return returnObjet;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<DataAccess.Models.ObjectBuilding> Upadate(string id, UpdateObjectBuilding objectBuilding)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    int intID = int.Parse(id);
                    var updatedVaue = ctx.ObjectBuildings.FirstOrDefault(x => x.Name == objectBuilding.Name);
                    if (updatedVaue != null && updatedVaue.Name == objectBuilding.Name && updatedVaue.ObjectId!=intID)
                        throw new Exception("Building with same name already exists");
                    var returnObjet = ctx.ObjectBuildings.FirstOrDefault(x=>x.ObjectId==intID);
                    returnObjet.Name = objectBuilding.Name;
                    returnObjet.Adress = objectBuilding.Adress;
                    returnObjet.City = objectBuilding.City;
                    ctx.SaveChanges();
                    return returnObjet;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        
    }
}
