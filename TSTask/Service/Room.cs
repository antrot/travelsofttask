﻿using DataAccess.Models;
using DTO.Request.Room;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class Room : IRoom
    {
        public string connectionString = Properties.Settings.Default.connectionString;

        public async Task<DataAccess.Models.Room> Create(CreateRoom room)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var single = ctx.Rooms.Where((System.Linq.Expressions.Expression<Func<DataAccess.Models.Room, bool>>)(x => x.RoomName == room.RoomName)).FirstOrDefault();
                    if (single != null)
                        throw new Exception("Room with same name alreeady exists in database");
                    int objectID = int.Parse(room.ObjectId);
                    var objectBuilding= ctx.ObjectBuildings.Where(x => x.ObjectId == objectID).FirstOrDefault();
                    if(objectBuilding==null)
                        throw new Exception("ObjectBuilding doesn't exists in database");


                    DataAccess.Models.Room saveRoom = new DataAccess.Models.Room()
                    {
                        RoomName = room.RoomName,
                        RoomDescription = room.RoomDescription,
                        ObjectId = objectBuilding.ObjectId,
                    };
                    ctx.Rooms.Add(saveRoom);
                    ctx.SaveChanges();
                    return saveRoom;
                }
                catch (Exception ex)
                {
                    var response = new DTO.Response.GenericResponse();
                    response.Response = ex.Message;
                    throw new Exception(ex.Message);
                }
            }

        }

        public async Task<DTO.Response.GenericResponse> Delete(string id)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var intID = int.Parse(id);

                    var single = ctx.Rooms.Where(x => x.RoomId == intID).FirstOrDefault();
                    ctx.Entry(single).State = System.Data.Entity.EntityState.Deleted;
                    ctx.SaveChanges();

                    var response = new DTO.Response.GenericResponse();
                    response.Response = "Sucess";
                    return response;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<DataAccess.Models.Room> Get(string id)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var intID = int.Parse(id);

                    var returnObjet = ctx.Rooms.Where(x => x.RoomId == intID).FirstOrDefault();
                    return returnObjet;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<List<DataAccess.Models.Room>> GetAll()
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var returnObjet = ctx.Rooms.ToList();
                    return returnObjet;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<DataAccess.Models.Room> Upadate(string id, UpdateRoom room)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var intID = int.Parse(id);
                    var single = ctx.Rooms.Where((System.Linq.Expressions.Expression<Func<DataAccess.Models.Room, bool>>)(x => x.RoomId == intID)).FirstOrDefault();
                    if (single == null)
                        throw new Exception("Room with that id does not exist");

                    single.RoomDescription = room.RoomDescription;
                    single.RoomName = room.RoomName;
                    ctx.SaveChanges();
                    return single;
                }
                catch (Exception ex)
                {
                    var response = new DTO.Response.GenericResponse();
                    response.Response = ex.Message;
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
