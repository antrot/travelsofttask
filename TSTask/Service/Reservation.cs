﻿using DataAccess.Models;
using DTO.Request.Reservation;
using Service.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service
{
    public class Reservation : IReservation
    {

        public async Task<DataAccess.Models.Reservation> Create(CreateReservation reservationCreate)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var intGuestID = int.Parse(reservationCreate.GuestId);
                    var intRoomID = int.Parse(reservationCreate.RoomId);

                    DataAccess.Models.Reservation reservation = new DataAccess.Models.Reservation()
                    {
                        DateFrom=reservationCreate.DateFrom,
                        DateTo=reservationCreate.DateTo,
                        GuestId= intGuestID,
                        RoomId= intRoomID
                    };
                    ctx.Reservations.Add(reservation);
                    ctx.SaveChanges();

                    var response = new DTO.Response.GenericResponse();
                    response.Response = "Sucess";
                    return reservation;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<DTO.Response.GenericResponse> Delete(string id)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var intID = int.Parse(id);

                    var single = ctx.Reservations.Where(x => x.ReservationId == intID).FirstOrDefault();
                    ctx.Entry(single).State = System.Data.Entity.EntityState.Deleted;
                    ctx.SaveChanges();

                    var response = new DTO.Response.GenericResponse();
                    response.Response = "Sucess";
                    return response;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<DataAccess.Models.Reservation> Get(string id)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var intID = int.Parse(id);
                    var returnObjet = ctx.Reservations.Where(x => x.RoomId == intID).FirstOrDefault();
                    return returnObjet;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<List<DataAccess.Models.Reservation>> GetAll()
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var returnObjet = ctx.Reservations.ToList();
                    return returnObjet;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public async Task<DataAccess.Models.Reservation> Upadate(string id, UpdateReservation reservationUpdate)
        {
            using (var ctx = new DataAccess.DatabaseContext.AplicationContext())
            {
                try
                {
                    var intID = int.Parse(id);
                    var reservation = ctx.Reservations.FirstOrDefault(x => x.ReservationId == intID);
                    if (reservation == null)
                        throw new Exception("Reservation with that ID does not exist");

                    reservation.DateFrom = reservationUpdate.DateFrom;
                    reservation.DateTo = reservationUpdate.DateTo;
                    ctx.SaveChanges();

                    var response = new DTO.Response.GenericResponse();
                    response.Response = "Sucess";
                    return reservation;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
